export const persona = {
    nombre: 'Juan Nadie',
    poblacion: 'Mondoñedo',
    edad: 56
}

export function diHola(nombre) {
    return `Hola ${nombre}`;
}